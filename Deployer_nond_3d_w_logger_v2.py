
import time
import numpy as np
# Timer
from timeit import default_timer as timer
# Dronekit, OMG tools
from dronekit import connect, VehicleMode, LocationGlobalRelative, mavutil
from omgtools import *


# GLOBALS ----------------------------------------------------------------------

#points = [[10., 0., 0], [15., 10., -1.]];    # NED!!!
#points = [[10., 0., 3], [15., 10., 4.]];    # NED!!!
points = [[7., 7., -3.], [0., 7., -2.], [0., 7., -3.], [0., 0., -3.]];    # NED!!!
#points = [[7., 7., 0], [0., 7., 1.], [0., 7., 0.], [0., 0., 0.]];    # NED!!!

# Altitude within the bellow interval is accepted as correct (relative value)
altitude_mismatch_factor = 0.95

# Copter vertical position
current_altitude = 0;
target_altitude = 3;


# dronekit ---------------------------------------------------------------------
def send_ned_velocity(vehicle_drone, velocity_x, velocity_y, velocity_z):
    """
    Move vehicle_drone in direction based on specified velocity vectors.
    x = north!!!
    """
    msg = vehicle_drone.message_factory.set_position_target_local_ned_encode(
        0,       # time_boot_ms (not used)
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_FRAME_LOCAL_NED, # frame
        0b0000111111000111, # type_mask (only speeds enabled)
        0, 0, 0, # x, y, z positions (not used)
        velocity_x, velocity_y, velocity_z, # x, y, z velocity in m/s
        0, 0, 0, # x, y, z acceleration (not supported, ignored in GCS_Mavlink)
        0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
    vehicle_drone.send_mavlink(msg);


# Omgtools ---------------------------------------------------------------------

# Create OMG vehicle
def init_vehicle_omg():

    vehicle_omg_shape = Plate(Rectangle(0.5, 1.), height=0.1);
    vehicle_omg = Holonomic3D(vehicle_omg_shape);
    # Set vehicle options and constraints
    vehicle_omg.set_options(
        {'ideal_prediction': False, 'safety_distance': 0.1});
     # Dummy points for problem.init()
    vehicle_omg.set_initial_conditions([0., 0., 0.]);
    vehicle_omg.set_terminal_conditions([0., 0., 0.]);
    return vehicle_omg;


# Create OMG problem and deployer
def init_problem_and_deployer (
    vehicle_omg, environment, _horizon_time=15, _sample_time=.1, _update_time=1):

    # Define horizon time and solver
    options = {'horizon_time': _horizon_time, \
        'solver_options': {'ipopt':{'ipopt.linear_solver': 'ma57'}}};
    # Create a point-to-point problem
    problem = Point2point \
        (vehicle_omg, environment, options=options, freeT=False);
    problem.init();
    # Create deployer using above problem
    deployer = Deployer(problem, _sample_time, _update_time);
    return [deployer, problem];


# Reset deployer (on each new via point)
def reset_deployer (deployer, vehicle_omg, via_point):

    # Set initial gues
    vehicle_omg.set_terminal_conditions(via_point);
    vehicle_omg.set_initial_conditions(via_point);
    # Reset deployer using initial guess
    deployer.reset();
    return [deployer, vehicle_omg];


# Call deployer -> recalculate trajectory on update_time
def update_speed_omg (
    deployer, problem, vehicle_drone,
    t00, via_point, n_samp, state_traj, input_traj, timing_traj,
    initial_east, initial_north, initial_down):

    # Calculate current state: compensate for inital relative offset
    current_state = np.array([ \
        vehicle_drone.location.local_frame.north - initial_north, \
        vehicle_drone.location.local_frame.east - initial_east, \
        vehicle_drone.location.local_frame.down - initial_down])
    #current_state = state_traj[:, -1]
    print "* Update time, recalculate velocity array:" \
          "\n\tCurrent state: %s" \
          "\n\tRelative position: %s" % \
            (str(current_state),
            str([vehicle_drone.location.local_frame.east,
                vehicle_drone.location.local_frame.north,
                vehicle_drone.location.local_frame.down]));

    # Update motion planning
    start_time = timer();
    # Calculation time time since movement began


    t0 = timer() - t00

    print t0
    print current_state

    trajectories = deployer.update(t0, current_state)

    epoch_time = timer();
    calc_time = round(epoch_time - start_time, 3);
    print "* Calculation time: ", calc_time;

    # Store state & input trajectories -> simulation of ideal trajectory following
    state_traj = np.c_[state_traj, trajectories['state'][:, 1:n_samp+1]]
    #state_traj = np.c_[state_traj[:,-1], trajectories['state'][:, 1:n_samp+1]]
    input_traj = np.c_[input_traj, trajectories['input'][:, 1:n_samp+1]]
    #input_traj = np.c_[input_traj[:,-1], trajectories['input'][:, 1:n_samp+1]]

    #return [deployer, problem, position, speed, end_movement_flag];
    return [deployer, problem, state_traj, input_traj, timing_traj, calc_time];


# matplotlib -------------------------------------------------------------------
def draw (state_traj, input_traj, sample_time) :
    n_t = state_traj.shape[1]
    _time = np.linspace(0., n_t*sample_time, n_t)

    _main_filepath =  os.path.abspath(__file__);
    _main_dirpath = os.path.dirname(_main_filepath);

    figtime = str(int(timer()));
    figname1 = figtime + "deployer_nondeterministic_3d_1.png";
    figname2 = figtime + "deployer_nondeterministic_3d_2.png";
    figname3 = figtime + "deployer_nondeterministic_3d_3.png";

    plt.figure()
    plt.subplot(3, 1, 1)
    plt.gca().set_title('state_traj 0')
    plt.plot(_time, state_traj[0, :])
    plt.subplot(3, 1, 2)
    plt.gca().set_title('state_traj 1')
    plt.plot(_time, state_traj[1, :])
    plt.subplot(3, 1, 3)
    plt.gca().set_title('state_traj 2')
    plt.plot(_time, state_traj[2, :])
    plt.savefig(os.path.join(_main_dirpath, figname1));

    plt.figure()
    plt.subplot(3, 1, 1)
    plt.gca().set_title('input_traj 0')
    plt.plot(_time, input_traj[0, :])
    plt.subplot(3, 1, 2)
    plt.gca().set_title('input_traj 1')
    plt.plot(_time, input_traj[1, :])
    plt.subplot(3, 1, 3)
    plt.gca().set_title('input_traj 2')
    plt.plot(_time, input_traj[2, :])
    plt.savefig(os.path.join(_main_dirpath, figname2));

    plt.figure()
    plt.subplot(2, 1, 2)
    plt.gca().set_title('input_traj xy')
    plt.plot(state_traj[0, :], state_traj[1, :])
    plt.subplot(2, 2, 1)
    plt.gca().set_title('input_traj xz')
    plt.plot(state_traj[0, :], state_traj[2, :])
    plt.subplot(2, 2, 2)
    plt.gca().set_title('input_traj yz')
    plt.plot(state_traj[1, :], state_traj[2, :])
    plt.savefig(os.path.join(_main_dirpath, figname3));



# MAIN -------------------------------------------------------------------------
def main():


    # Omgtools update and sample time
    sample_time = 0.2;
    update_time = 1;
    horizon_time = 15;
    n_samp = int(np.round(update_time/sample_time, 6));


    # End point reached, stop movement
    end_movement_flag = False;
    # Trajectory position and speed
    state_traj = np.c_[[0., 0., 0.]];
    input_traj = np.c_[[0., 0., 0.]];
    timing_traj = np.c_[[timer(), 0., 0.]];
    timing_traj_2 = np.c_[[0., 0., 0.]];
    position_traj = np.c_[[0., 0., 0.]];
    velocity_traj = np.c_[[0., 0., 0.]];

    # Relative position offset, saved at the beginning of a movement
    initial_north = 0;
    initial_east = 0;
    initial_down = 0;

    # Initialize dronekit vehicle and prepare for movement
    vehicle_drone = connect('udp:127.0.0.1:14550', wait_ready=True)

    # Do not try to arm until autopilot is ready
    print "* Waiting for vehicle to initialise (boot, GPS fix, EKF pre-arm)";
    while not vehicle_drone.is_armable:
        time.sleep(0.5);

    # Copter should arm in GUIDED mode
    print "* Arming motors"
    vehicle_drone.mode    = VehicleMode("GUIDED")
    vehicle_drone.armed   = True

    # Confirm vehicle armed before attempting to take off
    while not vehicle_drone.armed:
        print "* Waiting for arming..."
        time.sleep(.5)
    print "* Vehicle armed"


    # On Drone start, set relative starting position
    initial_north = vehicle_drone.location.local_frame.north
    initial_east = vehicle_drone.location.local_frame.east
    initial_down = vehicle_drone.location.local_frame.down
    print "* Initial north: %f\n" \
        "\tinitial east: %f\n" \
        "\tinitial down: %f\n" % \
        (initial_north, initial_east, initial_down);


    # Takeoff
    delta_altitude = target_altitude * (1-altitude_mismatch_factor);
    print "* Takeoff started, goal at relative alt: %fm +-%fm\n" % \
        (target_altitude, delta_altitude);
    vehicle_drone.simple_takeoff(target_altitude)

    current_altitude = vehicle_drone.location.global_relative_frame.alt;

    # Loop while taking off
    while not (abs(target_altitude - current_altitude) <= delta_altitude):
        time.sleep(0.1);
        current_altitude = vehicle_drone.location.global_relative_frame.alt;

    print "* Reached target altitude at: %fm +-%fm\n" % \
        (current_altitude, delta_altitude);

    max_dimension = 0;
    for point in points:
        if ((max([abs(xy) for xy in point]) > max_dimension)):
            max_dimension = max([abs(xy) for xy in point]);

    room_size = float(max_dimension + 5)*2;
    environment = Environment(room={'shape': Cube(room_size)})

    """
    environment.add_obstacle(Obstacle(
        {'position': [3.5, 3.5, -3.]}, shape=RegularPrisma(0.5, 0.5, 6)))

    environment.add_obstacle(Obstacle(
        {'position': [3.5, 7, -2.5]}, shape=Sphere(0.5)))
    """

    # initialize omgtools vehicle
    vehicle_omg = init_vehicle_omg();
    print "* OMG Vehicle initialised";

    [deployer, problem] = init_problem_and_deployer (
        vehicle_omg, environment, _horizon_time = horizon_time, \
        _sample_time = sample_time, _update_time = update_time);
    print "* OMG Deployer and Problem initialised";



    # LOOP POINTS ***

    # Point counter
    point_i = 0;

    for point in points:

        # Save 'zero' time for first point
        if point_i == 0:
            timing_traj = np.c_[[timer(), 0., 0.]];
        point_i += 1

        print "\n* NEW POINT\n" \
            "\tvia-point: %s\n" \
            "\tinitial north: %f\n\tinitial east: %f\n" \
            "\tinitial down: %f\n" % \
            (str(point), initial_north, initial_east, initial_down);

        [deployer, vehicle_omg] = reset_deployer (deployer, vehicle_omg, point);
        print "* OMG Deployer reset";

        # Reset counter (calculate new trajectory)
        k=0;

        # Initial time (for each point)
        t00 = timer();

        [deployer, problem, state_traj, input_traj, timing_traj, calc_time] = \
            update_speed_omg (
                deployer, problem, vehicle_drone,
                t00, point, n_samp, state_traj, input_traj, timing_traj,
                initial_east, initial_north, initial_down);

        # Interval (sample_time) timer
        t_start = timer();
        t_next = t_start + sample_time;

        while True:

            # Add times to timing array
            epoch_time = timer();
            elapsed_time = epoch_time - timing_traj[0,-1];
            timing_arr = np.c_[[epoch_time, elapsed_time, calc_time]];
            # Add drone position to position array
            position_arr = np.c_[[
                vehicle_drone.location.local_frame.north \
                    - initial_north,
                vehicle_drone.location.local_frame.east \
                    - initial_east,
                vehicle_drone.location.local_frame.down \
                    - initial_down
            ]];
            # Add drone velocity to velocity array
            velocity_arr = np.c_[vehicle_drone.velocity];

            # Check if via point was reached
            # *Change array shape from [[...],[...],[...]] to [..., ..., ...]

            if (np.linalg.norm(point-position_arr[:, 0]) < \
              1e-1 and np.linalg.norm(velocity_arr[:, 0]) < 1e-1):

                print "\n* Trajectory calculation reached end\n"  \
                    "\tvia point: %s\n"  \
                    "\tn_samp: %f\n"  \
                    "\tk: %f\n" \
                    "\t(n_samp-1-k): %f" % \
                    (str(point), n_samp, k, (n_samp-1-k));

                print "* Shapes: ", \
                    np.shape(timing_traj), np.shape(timing_traj_2), \
                    np.shape(position_traj), np.shape(velocity_traj), \
                    np.shape(state_traj), np.shape(input_traj);

                # Erase unused trajectories
                # *Avoid case where array[:,0:-0] -> results in empty array

                #retro_factor = n_samp-1-k
                retro_factor = n_samp-k

                #if (n_samp-1-k) != 0:
                if retro_factor > 0:
                    state_traj = state_traj[:,0:-(retro_factor)];
                    input_traj = input_traj[:,0:-(retro_factor)];
                break;
            elif (problem.iteration > 300):
                print "* Trajectory calculation abort: %s ITR" % \
                    str(problem.iteration);
                break;

            # Send movement command to drone
            send_ned_velocity(vehicle_drone, \
                input_traj[0,-n_samp+k],
                input_traj[1,-n_samp+k],
                input_traj[2,-n_samp+k]
            );

            # Add data arrays to master array (collection, working memory)
            timing_traj = np.c_[timing_traj, timing_arr];
            position_traj = np.c_[position_traj, position_arr];
            velocity_traj = np.c_[velocity_traj, velocity_arr];

            # Increment trajectory matrix array
            k = k+1;
            # Reset counter and calculate new trajectories
            if k >= n_samp:
                k = 0;

                # Start of interval (update_time), calculate trajectories
                [deployer, problem, state_traj, input_traj, timing_traj, calc_time] = \
                    update_speed_omg (
                        deployer, problem, vehicle_drone,
                        t00, point, n_samp, state_traj, input_traj, timing_traj,
                        initial_east, initial_north, initial_down);
                print "* Shapes after D: ", \
                    np.shape(timing_traj), np.shape(timing_traj_2), \
                    np.shape(position_traj), np.shape(velocity_traj), \
                    np.shape(state_traj), np.shape(input_traj);

            t_diff = t_next - timer()
            # Sleep till start of next period, if needed
            if (t_next - timer() > 0):
                time.sleep(t_next - timer());
            # Reset start time and next interval time
            t_start = timer();
            t_next = t_start + sample_time;

            # Save sleep times
            timing_arr_2 = np.c_[[t_start, t_start, t_diff]];
            timing_traj_2 = np.c_[timing_traj_2, timing_arr_2];


    print "\n* End position is: %s\n" % \
        str([vehicle_drone.location.local_frame.north \
                - initial_north,
            vehicle_drone.location.local_frame.east \
                - initial_east,
            vehicle_drone.location.local_frame.down \
                - initial_down]);

    # Write log file
    initial_time = timing_traj[0,0];
    logname = "%d.csv" % initial_time;
    with open(logname, "w") as log:

        #log.write("points, update time, sample time, ")
        #log.write("%s, %f, %f" % str(points), update_time, sample_time);
        #log.write("*");
        log.write("epoch, elapsed, calc, t_start, t_next, t_diff, " \
            "p_x, p_y, p_z, v_x, v_y, v_z, s_x, s_y, s_z, i_x, i_y, i_z" + "\n");

        n = np.shape(timing_traj)[1];
        for i in range(0,n):
            text = "";
            # Time
            text += (str(timing_traj[0,i]) + ",");
            text += (str(timing_traj[1,i]) + ",");
            text += (str(timing_traj[2,i]) + ",");
            # Time 2
            text += (str(timing_traj_2[0,i]) + ",");
            text += (str(timing_traj_2[1,i]) + ",");
            text += (str(timing_traj_2[2,i]) + ",");
            # Position
            text += (str(position_traj[0,i]) + ",");
            text += (str(position_traj[1,i]) + ",");
            text += (str(position_traj[2,i]) + ",");
            # Velocity
            text += (str(velocity_traj[0,i]) + ",");
            text += (str(velocity_traj[1,i]) + ",");
            text += (str(velocity_traj[2,i]) + ",");
            # State
            text += (str(state_traj[0,i]) + ",");
            text += (str(state_traj[1,i]) + ",");
            text += (str(state_traj[2,i]) + ",");
            # Speed
            text += (str(input_traj[0,i]) + ",");
            text += (str(input_traj[1,i]) + ",");
            text += (str(input_traj[2,i]));
            log.write(text + "\n");


    print "* Shapes: ", \
        np.shape(timing_traj), np.shape(timing_traj_2), \
        np.shape(position_traj), np.shape(velocity_traj), \
        np.shape(state_traj), np.shape(input_traj);

    #draw(state_traj, input_traj, sample_time);


if __name__ == "__main__":
    main();
