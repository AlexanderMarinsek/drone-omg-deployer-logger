## Multirotor guidance using OMG-tools, with added speed and position logging
**Written in Python, based on OMG-tools and Dronekit.**

Guides the multirotor using OMG-tools' motion planning module deployer and
while doing so, logs current position and speed, as well as the deployer's
estimate position and speed.

*Tested on Ardupilot's SITL simulator, and Pixhawk + DJI F550.

---

https://bitbucket.org/AlexanderMarinsek/drone-omg-deployer-logger/
git clone git@bitbucket.org:AlexanderMarinsek/drone-omg-deployer-logger.git
